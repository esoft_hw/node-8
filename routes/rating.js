const express = require('express');
const router = express.Router();

// Dummy data for player ratings
const playerRatings = [
  { name: 'John', games: 10, wins: 6, losses: 4 },
  { name: 'Jane', games: 12, wins: 8, losses: 4 },
  { name: 'Mike', games: 15, wins: 9, losses: 6 },
  { name: 'Sara', games: 8, wins: 3, losses: 5 }
];

// Route for displaying player ratings
router.get('/rating', (req, res) => {
  // Calculate percentage of wins for each player
  for (let player of playerRatings) {
    player.percentage = ((player.wins / player.games) * 100).toFixed(2);
  }
  
  res.render('rating', { players: playerRatings });
});

module.exports = router;