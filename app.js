const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const app = express();
const port = 3000;
const path = require('path')

const users = [
  {
    username: 'user1@bk.ru',
    password: '1' // password: secret1
  },
  {
    username: 'user2',
    password: 'secret2' // password: secret2
  },
];

app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.set('viwes', path.join(__dirname + 'viwes'))

app.get('/', (req, res) => {
  res.render('pages/authorization');
});

app.post('/authorization', (req, res) => {
  const { username, password } = req.body;

  const user = users.find((user) => { return user.username === username && user.password === password});

  if (!user) {
    return res.status(401).send('Invalid username or password');
  }

  res.redirect('/rating');
  // res.send('Всё окей!')
});

app.get('/rating', (req, res) => {
  res.render('pages/rating');
});

app.get('/authorization', (req, res) => {
  res.render('pages/authorization');
});

app.get('/index', (req, res) => {
  res.render('pages/index');
});


// const ratingRouter = require('./routes/rating');
// app.use('/rating', ratingRouter);

const playerRatings = [
  { name: 'John', games: 10, wins: 6, losses: 4, percentage: 0 },
  { name: 'Jane', games: 12, wins: 8, losses: 4, percentage: 0 },
  { name: 'Mike', games: 15, wins: 9, losses: 6, percentage: 0 },
  { name: 'Sara', games: 8, wins: 3, losses: 5, percentage: 0 }
];

// Route for displaying player ratings
app.get('/rating', (req, res) => {
  for (let player of playerRatings) {
    player.percentage = ((player.wins / player.games) * 100).toFixed(2);
  }

  res.render('pages/rating', { players: playerRatings });
});


// app.get('/', (req, res) => {
//   // You can define an array of users with their ratings here
//   const users = [
//     {
//       name: 'John Doe',
//       games: 10,
//       wins: 5,
//       losses: 5,
//       winPercentage: 50
//     },
//     {
//       name: 'Jane Doe',
//       games: 20,
//       wins: 10,
//       losses: 10,
//       winPercentage: 50
//     },
//     {
//       name: 'Bob Smith',
//       games: 15,
//       wins: 8,
//       losses: 7,
//       winPercentage: 53.33
//     }
//   ];

//   // Render the ratings.ejs template and pass in the users array as a parameter
//   res.render('ratings', { users: users });
// });


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

