function SubmitForm() {
    let loginInput = document.getElementById("login");
    let passwordInput = document.getElementById("password");

    const loginValue = loginInput.value.trim();
    const passwordValue = passwordInput.value.trim();

    const forValidTestLogin = /^[a-zA-Z0-9._@]+$/;
    const forValidTestPassword = /^[a-zA-Z0-9._]+$/;

    let isValidLogin = forValidTestLogin.test(loginValue);
    let isValidPassword = forValidTestPassword.test(passwordValue);

    if(!isValidLogin) {
        console.error("Невалидный логин!");
        return;
    }

    else if(!isValidPassword) {
        console.error("Невалидный пароль!");
        return;
    }

    else {
        console.log(`Логин: ${loginValue} , пароль: ${passwordValue}`);
    }

    loginInput.value = "";
    passwordInput.value = "";
}